import Vue from 'vue'
import VueRouter from 'vue-router'

const lazyLoad = (component) => import(/* webpackChunkName: "[request]" */ `@/views/${component}.vue`)

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => lazyLoad('Home')
  },
  {
    path: '/about',
    name: 'About',
    component: () => lazyLoad('About')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
