import { ls } from '@/assets/js/storage'
import { parsify } from '@/assets/js/parsify'

import _has from 'lodash/has'
import _isEmpty from 'lodash/isEmpty'

const helper = () => {
  return {
    csrftoken () {
      return ls.get('csrftoken') || ''
    },

    credentials (key = null) {
      const user = parsify(ls.get('user'))

      if (!user || !key) return null

      return user[key] || null
    }
  }
}

export const user = helper()
