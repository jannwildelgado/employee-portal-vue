export const parsify = str => {
  let result = str

  if (typeof str === 'string') {
    try {
      result = JSON.parse(result)
    } catch (error) {
      console.error('Error Parsing JSON Data')
      result = str[0] === '{' ? {} : []
    }
  }

  return result
}
