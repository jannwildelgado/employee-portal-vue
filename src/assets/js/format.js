import { parsify } from '@/assets/js/parsify'

import moment from 'moment'

export const formatter = (value, key = null) => {
  if (key === 'tags') {
    const list = !value ? [] : value.split(',')
    return list.map(e => e.split(' ').join(''))
  }

  if (key === 'image' && value) {
    return `${process.env.VUE_APP_API_URL}/public/${value}.jpg`
  }

  if (key === 'description' && value) {
    return value.length > 100 ? value.substring(0, 100).trim() + '...' : value
  }

  if (key === 'created' && value) {
    return moment(value).fromNow()
  }

  if (key === 'name' && value) {
    value = parsify(value)

    return (value && value.name) || ''
  }

  if (['id', 'name', 'img', 'bg', 'alias'].indexOf(key) !== -1) {
    value = parsify(value)

    try {
      return value[key] || ''
    } catch (err) {
      return null
    }
  }

  return null
}
