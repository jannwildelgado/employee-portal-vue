import _isEmpty from 'lodash/isEmpty'

const app = String(process.env.VUE_APP_NAME || 'app').toLocaleLowerCase()

const getKey = key => `${app}:${key}`

const storage = () => {
  return {
    set (key, value = null) {
      if (_isEmpty(key)) {
        return console.error('LocalStorage set Key is empty.')
      }

      key = getKey(key)
      localStorage.setItem(key, value)
    },

    get (key) {
      if (_isEmpty(key)) {
        return console.error('LocalStorage get Key is empty.')
      }

      key = getKey(key)
      return localStorage.getItem(key)
    },

    remove (key) {
      if (_isEmpty(key)) {
        return console.error('LocalStorage remove Key is empty.')
      }

      key = getKey(key)
      localStorage.removeItem(key)
    },

    clear () {
      return localStorage.clear()
    }
  }
}

export const ls = storage()
