import camelCase from 'lodash/camelCase'

const files = require.context('./modules', false, /\.js$/)
const modules = {}

files.keys().forEach(module => {
  const moduleName = camelCase(module.replace(/(\.\/|\.js)/g, ''))
  modules[moduleName] = files(module).default
})

export default modules
