const path = require('path')

module.exports = {
  productionSourceMap: false,

  parallel: 4,

  transpileDependencies: [
    'vuetify'
  ],

  devServer: {
    open: 'Google Chrome',
    port: 8084,
    host: '0.0.0.0',
    hot: true
  },

  configureWebpack: {
    module: {
      rules: [
        {
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: path.resolve(__dirname, 'node_modules'),
          options: {
            emitWarning: true,
            failOnError: true,
            failOnWarning: true
          }
        },
        {
          test: /\.(js)$/,
          loader: 'babel-loader',
          exclude: path.resolve(__dirname, 'node_modules')
        }
      ]
    }
  },

  lintOnSave: 'default'
}